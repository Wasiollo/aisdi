#ifndef AISDI_LINEAR_LINKEDLIST_H
#define AISDI_LINEAR_LINKEDLIST_H

#include <cstddef>
#include <initializer_list>
#include <stdexcept>

namespace aisdi
{

//******************************************************************************
//MAIN CLASS
//******************************************************************************

template <typename Type>
class LinkedList
{
public:
  using difference_type = std::ptrdiff_t;
  using size_type = std::size_t;
  using value_type = Type;
  using pointer = Type*;
  using reference = Type&;
  using const_pointer = const Type*;
  using const_reference = const Type&;

  class ConstIterator;
  class Iterator;
  using iterator = Iterator;
  using const_iterator = ConstIterator;

  //DONE
  LinkedList()
  {
    //calls default constructor for End automatically

    size = 0;
    First = &End;
  }

  //construct from initializer list
  LinkedList(std::initializer_list<Type> l) : LinkedList()
  {
    //(void)l; // disables "unused argument" warning, can be removed when method is implemented.
    //throw std::runtime_error("TODO");

    for(auto it = l.begin(); it != l.end(); ++it)
      append(*it);
  }

  //copy constructor
  LinkedList(const LinkedList& other) : LinkedList()
  {
    //(void)other;
    //throw std::runtime_error("TODO");

    for(auto it = other.cbegin(); it != other.cend(); ++it)
      append(*it);
  }

  //move constructor
  LinkedList(LinkedList&& other) : LinkedList()
  {
    //(void)other;
    //throw std::runtime_error("TODO");

    size = other.size;

    if(other.First != &(other.End))
      First = other.First;

    End.prev = other.End.prev;

    //when moved list non-empty
    if(End.prev)
      End.prev->next = &End;

    other.First = &(other.End);
    other.End.prev = NULL;
    other.size = 0;
  }

  //DONE
  ~LinkedList()
  {
    erase(cbegin(), cend());
  }

  //asignment operator
  LinkedList& operator=(const LinkedList& other)
  {
    //(void)other;
    //throw std::runtime_error("TODO");

    erase(cbegin(), cend());

    for(auto it = other.cbegin(); it != other.cend(); ++it)
      append(*it);

    return *this;
  }

  //move assignment operator
  LinkedList& operator=(LinkedList&& other)
  {
    //(void)other;
    //throw std::runtime_error("TODO");

    erase(cbegin(), cend());

    size = other.size;

    if(other.First != &(other.End))
      First = other.First;

    End.prev = other.End.prev;

    if(End.prev)
      End.prev->next = &End;

    other.First = &(other.End);
    other.End.prev = NULL;
    other.size = 0;

    return *this;
  }

  //DONE
  inline bool isEmpty() const
  {
    //std::runtime_error("TODO");
    //throw std::runtime_error("TODO");

    return First == &End;
  }

  //DONE
  inline size_type getSize() const
  {
    //throw std::runtime_error("TODO");

    return size;
  }

  //DONE
  void append(const Type& item)
  {
    //(void)item;
    //throw std::runtime_error("TODO");

    ++size;

    End.prev = new DataNode(item, End.prev, &End);

    //appending empty list
    if(First == &End)
      First = End.prev;

    //appending non-empty list
    else
      End.prev->prev->next = End.prev;
  }

  //DONE
  void prepend(const Type& item)
  {
    //(void)item;
    //throw std::runtime_error("TODO");

    ++size;

    First = new DataNode(item, NULL, First);

    First->next->prev = First;
  }

  //DONE
  void insert(const const_iterator& insertPosition, const Type& item)
  {
    //(void)insertPosition;
    //(void)item;
    //throw std::runtime_error("TODO");

    ++size;

    DataNode* tmp = new DataNode(item, insertPosition.ptr->prev, insertPosition.ptr);

    if(tmp->prev)
      tmp->prev->next = tmp;

    else
      First = tmp;

    tmp->next->prev = tmp;
  }

  Type popFirst()
  {
    //throw std::runtime_error("TODO");

    if(First == &End)
      throw std::logic_error("Attempted to pop an empty list");

    --size;

    Type ret = static_cast<DataNode*>(First)->data;

    First = First->next;

    delete First->prev;

    First->prev = NULL;

    return ret;
  }

  Type popLast()
  {
    //throw std::runtime_error("TODO");

    if(First == &End)
      throw std::logic_error("Attempted to pop an empty list");

    --size;

    Type ret = static_cast<DataNode*>(End.prev)->data;

    Node* del = End.prev;

    End.prev = End.prev->prev;

    if(End.prev)
      End.prev->next = &End;

    //popping last element
    else
      First = &End;

    delete del;

    return ret;
  }

  //DONE
  void erase(const const_iterator& position)
  {
    //(void)position;
    //throw std::runtime_error("TODO");

    if(First == &End)
      throw std::out_of_range("Attempted erase in an empty list");

    Node* del = position.ptr;

    if(!del->next)
      throw std::out_of_range("Attempted erase at end iterator");

    --size;

    //erasing non-first element
    if(del->prev)
      del->prev->next = del->next;

    //erasing first element
    else
      First = del->next;

    del->next->prev = del->prev;

    delete del;
  }

  //DONE
  void erase(const const_iterator& firstIncluded, const const_iterator& lastExcluded)
  {
    //(void)firstIncluded;
    //(void)lastExcluded;
    //throw std::runtime_error("TODO");

    if(firstIncluded == lastExcluded)
      return;

    if(firstIncluded.ptr->prev)
      firstIncluded.ptr->prev->next = lastExcluded.ptr;

    else
      First = lastExcluded.ptr;

    lastExcluded.ptr->prev = firstIncluded.ptr->prev;

    Node* i = firstIncluded.ptr;
    Node* del;

    while(i != lastExcluded.ptr)
    {
      --size;

      del = i;
      i = i->next;
      delete del;
    }
  }

  iterator begin()
  {
    //throw std::runtime_error("TODO");

    return Iterator(First);
  }

  iterator end()
  {
    //throw std::runtime_error("TODO");

    return Iterator(&End);
  }

  const_iterator cbegin() const
  {
    //throw std::runtime_error("TODO");

    return ConstIterator(First);
  }

  const_iterator cend() const
  {
    //throw std::runtime_error("TODO");

    return ConstIterator(const_cast<Node*>(&End));
  }

  const_iterator begin() const
  {
    return cbegin();
  }

  const_iterator end() const
  {
    return cend();
  }

//essential implementation elements
protected:

  //general Node blueprint
  struct Node
  {
    Node* prev;
    Node* next;

    Node(Node* p = NULL, Node* n = NULL) : prev(p), next(n) {}
  };

  //specific data-containing Node
  struct DataNode : Node
  {
    Type data;

    DataNode(Type t, Node* p, Node* n)
    {
      data = t;
      this->prev = p;
      this->next = n;
    }
  };

  Node* First;
  //Node* Last;
  Node End;

  size_type size;
};

//******************************************************************************
//CONST ITERATOR
//******************************************************************************

template <typename Type>
class LinkedList<Type>::ConstIterator
{
public:
  using iterator_category = std::bidirectional_iterator_tag;
  using value_type = typename LinkedList::value_type;
  using difference_type = typename LinkedList::difference_type;
  using pointer = typename LinkedList::const_pointer;
  using reference = typename LinkedList::const_reference;

  explicit ConstIterator(Node* p = NULL) : ptr(p)
  {}

  //dereference
  reference operator*() const
  {
    //throw std::runtime_error("TODO");

    if(!ptr->next)
      throw std::out_of_range("Attempted end iterator dereference");

    return static_cast<DataNode*>(ptr)->data;
  }

  //preincrement
  ConstIterator& operator++()
  {
    //throw std::runtime_error("TODO");

    if(!ptr->next)
      throw std::out_of_range("Attempted end iterator increment");

    ptr = ptr->next;

    return *this;
  }

  //postincrement
  ConstIterator operator++(int)
  {
    //throw std::runtime_error("TODO");

    if(!ptr->next)
      throw std::out_of_range("Attempted end iterator increment");

    ConstIterator ret = *this;

    ptr = ptr->next;

    return ret;
  }

  //predecrement
  ConstIterator& operator--()
  {
    //throw std::runtime_error("TODO");

    if(!ptr->prev)
      throw std::out_of_range("Attempted begin iterator decrement");

    ptr = ptr->prev;

    return *this;
  }

  //postdecrement
  ConstIterator operator--(int)
  {
    //throw std::runtime_error("TODO");

    if(!ptr->prev)
      throw std::out_of_range("Attempted begin iterator decrement");

    ConstIterator ret = *this;

    ptr = ptr->prev;

    return ret;
  }

  ConstIterator operator+(difference_type d) const
  {
    //(void)d;
    //throw std::runtime_error("TODO");

    ConstIterator ret = *this;

    while(d--)
    {
      if(!ret.ptr->next)
        throw std::out_of_range("Attempted arithmetic add operation out of list range");

      ret.ptr = ret.ptr->next;
    }

    return ret;
  }

  ConstIterator operator-(difference_type d) const
  {
    //(void)d;
    //throw std::runtime_error("TODO");

    ConstIterator ret = *this;

    while(d--)
    {
      if(!ret.ptr->prev)
        throw std::out_of_range("Attempted arithmetic substr operation out of list range");

      ret.ptr = ret.ptr->prev;
    }

    return ret;
  }

  bool operator==(const ConstIterator& other) const
  {
    //(void)other;
    //throw std::runtime_error("TODO");

    return ptr == other.ptr;
  }

  bool operator!=(const ConstIterator& other) const
  {
    //(void)other;
    //throw std::runtime_error("TODO");

    return ptr != other.ptr;
  }


protected:

  Node* ptr;

  friend void aisdi::LinkedList<Type>::insert(const ConstIterator&, const Type&);
  friend void aisdi::LinkedList<Type>::erase(const ConstIterator&);
  friend void aisdi::LinkedList<Type>::erase(const ConstIterator&, const ConstIterator&);
};

//******************************************************************************
//ITERATOR
//******************************************************************************

template <typename Type>
class LinkedList<Type>::Iterator : public LinkedList<Type>::ConstIterator
{
public:
  using pointer = typename LinkedList::pointer;
  using reference = typename LinkedList::reference;

  explicit Iterator(Node* p = NULL) : ConstIterator(p)
  {}

  //DONE
  Iterator(const ConstIterator& other)
    : ConstIterator(other)
  {}

  //DONE
  Iterator& operator++()
  {
    ConstIterator::operator++();
    return *this;
  }

  //DONE
  Iterator operator++(int)
  {
    auto result = *this;
    ConstIterator::operator++();
    return result;
  }

  //DONE
  Iterator& operator--()
  {
    ConstIterator::operator--();
    return *this;
  }

  //DONE
  Iterator operator--(int)
  {
    auto result = *this;
    ConstIterator::operator--();
    return result;
  }

  //DONE
  Iterator operator+(difference_type d) const
  {
    return ConstIterator::operator+(d);
  }

  //DONE
  Iterator operator-(difference_type d) const
  {
    return ConstIterator::operator-(d);
  }

  //DONE
  reference operator*() const
  {
    // ugly cast, yet reduces code duplication.
    return const_cast<reference>(ConstIterator::operator*());
  }
};

}

#endif // AISDI_LINEAR_LINKEDLIST_H
