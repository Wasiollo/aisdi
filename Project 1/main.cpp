#include <cstddef>
#include <cstdlib>
#include <string>
#include <iostream>

#include <chrono>

#include "Vector.h"
#include "LinkedList.h"

namespace
{

template <typename T>
using VV = aisdi::Vector<T>;

template <typename T>
using LL = aisdi::LinkedList<T>;

using clk = std::chrono::system_clock;
using timep = std::chrono::time_point<clk>;
using usec = std::chrono::microseconds;

struct timeRange
{
	timep start;
	timep end;
	
	timeRange(timep s, timep e) : start(s), end(e) {};
};

usec testTime(timeRange (*tested)())
{
	timeRange tr = tested();
	
	return std::chrono::duration_cast<usec>(tr.end - tr.start);
}

template <template <typename T> class Cl>
timeRange eraseCollection()
{
	Cl<int> coll;
	
	for(int i=0;i<10000;++i)
		coll.append(i);
	
	timep s = clk::now();
	
	coll.erase(coll.cbegin(), coll.cend());
	
	timep e = clk::now();
	
	return timeRange(s, e);
}

template <template <typename T> class Cl>
timeRange append()
{
	Cl<int> coll;
	
	timep s = clk::now();
	
	for(int i=0;i<10000;++i)
		coll.append(i);
		
	timep e = clk::now();
	
	return timeRange(s, e);
}

template <template <typename T> class Cl>
timeRange prepend()
{
	Cl<int> coll;
	
	timep s = clk::now();
	
	for(int i=0;i<10000;++i)
		coll.prepend(i);
		
	timep e = clk::now();
	
	return timeRange(s, e);
}

template <template <typename T> class Cl>
timeRange popFirst()
{
	Cl<int> coll;
	
	for(int i=0;i<10000;++i)
	 coll.append(i);
	 
	timep s = clk::now();
	
	while(!coll.isEmpty())
		coll.popFirst();
		
	timep e = clk::now();
	
	return timeRange(s, e);
}

template <template <typename T> class Cl>
timeRange popLast()
{
	Cl<int> coll;
	
	for(int i=0;i<10000;++i)
	 coll.append(i);
	 
	timep s = clk::now();
	
	while(!coll.isEmpty())
		coll.popLast();
		
	timep e = clk::now();
	
	return timeRange(s, e);
}

template <template <typename T> class Cl>
timeRange insert()
{
	//LEET BASS
	Cl<int> coll = {1337, 8455};
	
	timep s = clk::now();
	
	for(int i=0;i<10000;++i)
		coll.insert( ++(coll.cbegin()), i );
		
	timep e = clk::now();
	
	return timeRange(s, e);
}

void perfomTest()
{
  	//std::cout << std::fixed;
  	
  	std::cout << "[LIST TEST] :: [VECTOR TEST]" << std::endl;
  	
  	std::cout << std::endl;
  	
  	std::cout 	<< "erase test: " << testTime(eraseCollection<LL>).count() << " us :: " << testTime(eraseCollection<VV>).count() << " us"<< std::endl
  				<< "append test: "<< testTime(append<LL>).count() << " us :: " << testTime(append<VV>).count() << " us"<< std::endl
				<< "prepend test: "<< testTime(prepend<LL>).count() << " us :: " << testTime(prepend<VV>).count() << " us"<< std::endl
  				<< "popFirst test: "<< testTime(popFirst<LL>).count() << " us :: " << testTime(popFirst<VV>).count() << " us"<< std::endl
  				<< "popLast test: "<< testTime(popLast<LL>).count() << " us :: " << testTime(popLast<VV>).count() << " us"<< std::endl
  				<< "insert test: "<< testTime(insert<LL>).count() << " us :: " << testTime(insert<VV>).count() << " us"<< std::endl;
  				
  	std::cout << std::endl;
}

} // namespace

int main(int argc, char** argv)
{
  const std::size_t repeatCount = argc > 1 ? std::atoll(argv[1]) : 10000;
  for (std::size_t i = 0; i < repeatCount; ++i)
    perfomTest();
  return 0;
}
