  #ifndef AISDI_LINEAR_VECTOR_H
#define AISDI_LINEAR_VECTOR_H

#include <cstddef>
#include <initializer_list>
#include <stdexcept>

#define START_SIZE 8

namespace aisdi
{

//******************************************************************************
//MAIN CLASS
//******************************************************************************

template <typename Type>
class Vector
{
public:
  using difference_type = std::ptrdiff_t;
  using size_type = std::size_t;
  using value_type = Type;
  using pointer = Type*;
  using reference = Type&;
  using const_pointer = const Type*;
  using const_reference = const Type&;

  class ConstIterator;
  class Iterator;
  using iterator = Iterator;
  using const_iterator = ConstIterator;

  //default constructor
  Vector() : size(0), capacity(START_SIZE)
  {
    pastEnd = Buffer = new Type[START_SIZE];
  }

  Vector(std::initializer_list<Type> l) : Vector()
  {
    //(void)l; // disables "unused argument" warning, can be removed when method is implemented.
    //throw std::runtime_error("TODO");

    typename std::initializer_list<Type>::iterator it;

    for(it = l.begin(); it != l.end(); ++it)
      append(*it);
  }

  //copy constructor
  Vector(const Vector& other) : Vector()
  {
    //(void)other;
    //throw std::runtime_error("TODO");

    for(const_iterator it = other.cbegin(); it != other.cend(); ++it)
      append(*it);
  }

  //move constructor
  Vector(Vector&& other)
  {
    //(void)other;
    //throw std::runtime_error("TODO");

    size = other.size;
    capacity = other.capacity;

    Buffer = other.Buffer;
    pastEnd = other.pastEnd;

    //reinitialize other
    other.size = 0;
    other.capacity = START_SIZE;
    other.pastEnd = other.Buffer = new Type[START_SIZE];
  }

  //DONE
  ~Vector()
  {
    pastEnd = NULL;

    size = 0;
    capacity = 0;

    delete[] Buffer;

    Buffer = NULL;
  }

  //asignment operator
  Vector& operator=(const Vector& other)
  {
    //(void)other;
    //throw std::runtime_error("TODO");

    erase(cbegin(), cend());

    for(const_iterator it = other.cbegin(); it != other.cend(); ++it)
      append(*it);

    return *this;
  }

  //move assignment
  Vector& operator=(Vector&& other)
  {
    //(void)other;
    //throw std::runtime_error("TODO");

    //delete non-needed array of target (this) vector
    Type* del = Buffer;

    size = other.size;
    capacity = other.capacity;

    Buffer = other.Buffer;
    pastEnd = other.pastEnd;

    //reinitialize other
    other.size = 0;
    other.capacity = START_SIZE;
    other.pastEnd = other.Buffer = new Type[START_SIZE];

    delete[] del;

    return *this;
  }

  //DONE
  inline bool isEmpty() const
  {
    //std::runtime_error("TODO");
    //throw std::runtime_error("TODO");

    return !size;
  }

  //DONE
  inline size_type getSize() const
  {
    //throw std::runtime_error("TODO");

    return size;
  }

  void append(const Type& item)
  {
    //(void)item;
    //throw std::runtime_error("TODO");

    ++size;

    Type* tmp;

    //resize needed
    if( (tmp = resize()) )
    {
      Type* del = Buffer;

      Type* i;
      const_iterator it = cbegin();

      for(i = tmp; it != cend(); ++i, ++it)
        *i = *it;

      Buffer = tmp;
      pastEnd = i;

      delete[] del;
    }

    *(pastEnd++) = item;
  }

  void prepend(const Type& item)
  {
    //(void)item;
    //throw std::runtime_error("TODO");

    Type* tmp;

    //resize needed
    if( (tmp = resize()) )
    {
      Type* del = Buffer;

      *tmp = item;

      Type* i;
      const_iterator it = cbegin();

      for(i = tmp + 1; it != cend(); ++i, ++it)
        *i = *it;

      Buffer = tmp;
      pastEnd = i;

      delete[] del;
    }

    //right shift
    else
    {
      right_shift(cbegin());

      *Buffer = item;
    }

    //size must be incremented here for correct right_shift behaviour!!!
    ++size;
  }

  void insert(const const_iterator& insertPosition, const Type& item)
  {
    //(void)insertPosition;
    //(void)item;
    //throw std::runtime_error("TODO");

    if(insertPosition == cend())
    {
      append(item);
      return;
    }

    Type* tmp;

    //resize needed
    if( (tmp = resize()) )
    {
      Type* del = Buffer;

      Type* i;
      const_iterator it = cbegin();

      //copy from beg to insPos-1
      for(i = tmp; it != insertPosition; ++i, ++it)
        *i = *it;

      //insert element
      *i = item;

      //copy from insPos to end
      for(++i; it != cend(); ++i, ++it)
        *i = *it;

      Buffer = tmp;
      pastEnd = i;

      delete[] del;
    }

    //right shift
    else
    {
      right_shift(insertPosition);

      *( (iterator)insertPosition ) = item;
    }

    //size must be incremented here for correct right_shift behaviour!!!
    ++size;
  }

  Type popFirst()
  {
    //throw std::runtime_error("TODO");

    if(isEmpty())
      throw std::logic_error("Attempted pop in empty vector");

    Type ret = *Buffer;

    left_shift(cbegin(), cbegin()+1);

    --size;

    return ret;
  }

  Type popLast()
  {
    //throw std::runtime_error("TODO");

    if(isEmpty())
      throw std::logic_error("Attempted pop in empty vector");

    --size;

    return *(--pastEnd);
  }

  void erase(const const_iterator& position)
  {
    //(void)position;
    //throw std::runtime_error("TODO");

    if(isEmpty())
      throw std::out_of_range("Attempted erase in an empty vector");

    if(position == cend())
      throw std::out_of_range("Attempted erase at end iterator");

    if(position == cend()-1)
    {
      popLast();
      return;
    }

    left_shift(position, position+1);

    --size;
  }

  void erase(const const_iterator& firstIncluded, const const_iterator& lastExcluded)
  {
    //(void)firstIncluded;
    //(void)lastExcluded;
    //throw std::runtime_error("TODO");

    if(isEmpty())
      throw std::out_of_range("Attempted erase in an empty vector");

    if(firstIncluded == lastExcluded)
      return;

    size -= lastExcluded.ptr - firstIncluded.ptr;

    left_shift(firstIncluded, lastExcluded);
  }

  iterator begin()
  {
    //throw std::runtime_error("TODO")

    return Iterator(Buffer, *this);
  }

  iterator end()
  {
    //throw std::runtime_error("TODO");
    return Iterator(pastEnd, *this);
  }

  const_iterator cbegin() const
  {
    //throw std::runtime_error("TODO");

    return ConstIterator(Buffer, *this);
  }

  const_iterator cend() const
  {
    //throw std::runtime_error("TODO");

    return ConstIterator(pastEnd, *this);
  }

  const_iterator begin() const
  {
    return cbegin();
  }

  const_iterator end() const
  {
    return cend();
  }

protected:

  //left shift elements from pos_from to end
  //a block is shifted as to begin at pos_to
  void left_shift(const const_iterator& pos_to, const const_iterator& pos_from)
  {
    iterator to = Iterator(pos_to.ptr, *this);
    const_iterator from = pos_from;

    for(; from != cend(); ++to, ++from)
      *to = *from;

    pastEnd = to.ptr;
  }

  //right shift elements from end to pos by 1
  //check for valid capacity before calling!!!
  void right_shift(const const_iterator& pos)
  {
    //in an empty vector just relocate pastEnd
    if(isEmpty())
    {
      ++pastEnd;
      return;
    }

    iterator it = end()-1;

    //pastEnd must be incremented here to avoid (it+1)==pastEnd deref exception!!!
    ++pastEnd;

    for(; it != pos; --it)
      *(it+1) = *it;

    //once more for it=pos
    *(it+1) = *it;
  }

  //creates a new, bigger array in memory if needed
  //its returned pointer must always be assigned!!!
  //operates based on size updated beforehand!!!
  //doesn't update pointers!!!
  Type* resize()
  {
    if(size+1 <= capacity)
      return NULL;

    capacity = 2 * size;

    return new Type[capacity];
  }

protected:

  size_type size;
  size_type capacity;

  Type* Buffer;
  Type* pastEnd;
};

//******************************************************************************
//CONST ITERATOR
//******************************************************************************

template <typename Type>
class Vector<Type>::ConstIterator
{
public:
  using iterator_category = std::bidirectional_iterator_tag;
  using value_type = typename Vector::value_type;
  using difference_type = typename Vector::difference_type;
  using pointer = typename Vector::const_pointer;
  using reference = typename Vector::const_reference;

  explicit ConstIterator(Type* p, const Vector<Type>& v) : ptr(p), vec(v)
  {}

  reference operator*() const
  {
    //throw std::runtime_error("TODO");

    if(ptr == vec.pastEnd)
      throw std::out_of_range("Attempted end iterator dereference");

    return *ptr;
  }

  ConstIterator& operator++()
  {
    //throw std::runtime_error("TODO");

    if(ptr == vec.pastEnd)
      throw std::out_of_range("Attempted end iterator increment");

    ++ptr;

    return *this;
  }

  ConstIterator operator++(int)
  {
    //throw std::runtime_error("TODO");

    if(ptr == vec.pastEnd)
      throw std::out_of_range("Attempted end iterator increment");

    ConstIterator ret = *this;

    ++ptr;

    return ret;
  }

  ConstIterator& operator--()
  {
    //throw std::runtime_error("TODO");

    if(ptr == vec.Buffer)
      throw std::out_of_range("Attempted begin iterator decrement");

    --ptr;

    return *this;
  }

  ConstIterator operator--(int)
  {
    //throw std::runtime_error("TODO");

    if(ptr == vec.Buffer)
      throw std::out_of_range("Attempted begin iterator decrement");

    ConstIterator ret = *this;

    --ptr;

    return ret;
  }

  ConstIterator operator+(difference_type d) const
  {
    //(void)d;
    //throw std::runtime_error("TODO");

    if(ptr + d > vec.pastEnd)
      throw std::out_of_range("Attempted arithmetic add operation out of vector range");

    return ConstIterator(ptr + d, vec);
  }

  ConstIterator operator-(difference_type d) const
  {
    //(void)d;
    //throw std::runtime_error("TODO");

    if(ptr - d < vec.Buffer)
      throw std::out_of_range("Attempted arithmetic substr operation out of vector range");

    return ConstIterator(ptr - d, vec);
  }

  bool operator==(const ConstIterator& other) const
  {
    //(void)other;
    //throw std::runtime_error("TODO");

    return ptr == other.ptr;
  }

  bool operator!=(const ConstIterator& other) const
  {
    //(void)other;
    //throw std::runtime_error("TODO");

    return ptr != other.ptr;
  }

protected:

  Type* ptr;
  const Vector<Type>& vec;

  friend void aisdi::Vector<Type>::erase(const ConstIterator&, const ConstIterator&);
  friend void aisdi::Vector<Type>::left_shift(const ConstIterator&, const ConstIterator&);
};

//******************************************************************************
//ITERATOR
//******************************************************************************

template <typename Type>
class Vector<Type>::Iterator : public Vector<Type>::ConstIterator
{
public:
  using pointer = typename Vector::pointer;
  using reference = typename Vector::reference;

  explicit Iterator(Type* p, const Vector<Type>& v) : ConstIterator(p, v)
  {}

  Iterator(const ConstIterator& other)
    : ConstIterator(other)
  {}

  Iterator& operator++()
  {
    ConstIterator::operator++();
    return *this;
  }

  Iterator operator++(int)
  {
    auto result = *this;
    ConstIterator::operator++();
    return result;
  }

  Iterator& operator--()
  {
    ConstIterator::operator--();
    return *this;
  }

  Iterator operator--(int)
  {
    auto result = *this;
    ConstIterator::operator--();
    return result;
  }

  Iterator operator+(difference_type d) const
  {
    return ConstIterator::operator+(d);
  }

  Iterator operator-(difference_type d) const
  {
    return ConstIterator::operator-(d);
  }

  reference operator*() const
  {
    // ugly cast, yet reduces code duplication.
    return const_cast<reference>(ConstIterator::operator*());
  }
};

}

#endif // AISDI_LINEAR_VECTOR_H
