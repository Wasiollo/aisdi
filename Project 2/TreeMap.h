#ifndef AISDI_MAPS_TREEMAP_H
#define AISDI_MAPS_TREEMAP_H

#include <cstddef>
#include <initializer_list>
#include <stdexcept>
#include <utility>

namespace aisdi
{

template <typename KeyType, typename ValueType>
class TreeMap
{
public:
  using key_type = KeyType;
  using mapped_type = ValueType;
  using value_type = std::pair<const key_type, mapped_type>;
  using size_type = std::size_t;
  using reference = value_type&;
  using const_reference = const value_type&;

  class ConstIterator;
  class Iterator;
  using iterator = Iterator;
  using const_iterator = ConstIterator;

  TreeMap()
  {
    root=nullptr;
    size=0;
  }
  ~TreeMap()
  {
    remove_all();
  }

  TreeMap(std::initializer_list<value_type> list)
  {
    root = nullptr;
    size = 0;
    for (auto it = list.begin(); it != list.end(); ++it)
    {
      add((*it).first, (*it).second);
    }
  }

  TreeMap(const TreeMap& other)
  {
    root = nullptr;
    size = 0;
    for (auto it = other.begin(); it != other.end(); ++it)
    {
      add(it->first, it->second);
    }
  }

  TreeMap(TreeMap&& other)
  {
    root = other.root;
    size = other.size;
    other.root = nullptr;
    other.size = 0;
  }

  TreeMap& operator=(const TreeMap& other)
  {
    if (this != &other)
    {
      remove_all();
      for (auto it = other.begin(); it != other.end(); ++it)
      {
        add(it->first, it->second);
      }
    }
    return *this;
  }

  TreeMap& operator=(TreeMap&& other)
  {
    if (this != &other)
    {
        remove_all();
        root = other.root;
        size = other.size;
        other.root = nullptr;
        other.size = 0;
    }
    return *this;
  }

  bool isEmpty() const
  {
    return size==0;
  }

  mapped_type& operator[](const key_type& key)
  {
    if(size==0)
    {
        root= new Node(key,ValueType());
        ++size;
        return root->keyValPair.second;
    }
    Node* foundParent=find_parent(key);

    if(foundParent == nullptr)
        return root->keyValPair.second;

    if(foundParent->keyValPair.first > key)
    {
        if(foundParent->left == nullptr)//lets create node
        {
            foundParent->left = new Node(key,ValueType());
            ++size;
        }
        return foundParent->left->keyValPair.second;
    }
    else
    {
         if(foundParent->right == nullptr)//lets create node
        {
            foundParent->right = new Node(key,ValueType());
            ++size;
        }
        return foundParent->right->keyValPair.second;
    }
  }

  const mapped_type& valueOf(const key_type& key) const
  {
    const_iterator it = find(key);
    if (it == cend()) throw std::out_of_range("out of range");
    return it->second;
  }

  mapped_type& valueOf(const key_type& key)
  {
    iterator it = find(key);
    if (it == end()) throw std::out_of_range("out of range");
    return it->second;

  }

  const_iterator find(const key_type& key) const
  {
    Node* temp = root;
    while (temp != nullptr)
    {
      if (key == temp->keyValPair.first) return const_iterator(this,temp,false);

      if (key < temp->keyValPair.first)
        temp= temp->left;
      else if(key > temp->keyValPair.first)
        temp = temp->right;
    }
    return cend();
  }

  iterator find(const key_type& key)
  {
    Node* temp = root;
    while (temp != nullptr)
    {
      if (key == temp->keyValPair.first) return iterator(this,temp,false);

      if (key < temp->keyValPair.first)
        temp= temp->left;
      else if(key > temp->keyValPair.first)
        temp = temp->right;
    }
    return end();
  }

  void remove(const key_type& key)
  {
    const_iterator it= find(key);
    remove(it);
  }

  void remove(const const_iterator& it)
  {
    if(it==cend()) throw std::out_of_range("out of range");
    Node* toRemove=it.node;
    if(toRemove->left && toRemove->right)
    {
        Node* temp= toRemove->right;

        while(temp->left!=nullptr)
            temp=toRemove->left;

        if(toRemove->parent!=nullptr)
        {
            temp->parent->left=nullptr;
            temp->right=toRemove->right;
            temp->left=toRemove->left;
            temp->parent=toRemove->parent;

            toRemove->left->parent=temp;
            toRemove->right->parent=temp;

            if(toRemove->parent->left==toRemove)
                toRemove->parent->left=temp;
            else
                toRemove->parent->right=temp;
        }
        else
        {
            temp->parent->left=nullptr;
            temp->right=toRemove->right;
            temp->left=toRemove->left;

            toRemove->left->parent=temp;
            toRemove->right->parent=temp;
            temp->parent=nullptr;
            if(toRemove==root)
            root=temp;
        }
        --size;
        delete toRemove;
        return;
    }
    if(toRemove->left)
    {
        {
            root=toRemove->left;
            delete toRemove;
            --size;
            return;
        }
        toRemove->parent->left=toRemove->left;
        toRemove->left->parent=toRemove->parent;
        delete toRemove;
        --size;
        return;
    }
    if(toRemove->right)
    {
        if(toRemove==root)
        {
            root=toRemove->right;
            delete toRemove;
            --size;
            return;
        }
        toRemove->parent->right=toRemove->right;
        toRemove->right->parent=toRemove->parent;
        --size;
        delete toRemove;
        return;
    }
    else// dont have kids
    {
        --size;
        if(toRemove->parent)
        {
            if(toRemove->parent->left==toRemove)
                toRemove->parent->left=nullptr;
            else toRemove->parent->right=nullptr;

        }
        else root=nullptr;

        delete toRemove;
        return;
    }

  }

  size_type getSize() const
  {
    return size;
  }

  bool operator==(const TreeMap& other) const
  {
    if (size != other.size)
        return false;

    for (auto it = other.begin(); it != other.end(); ++it)
    {
      auto found = find(it->first);
      if ((found == end()) || (found->second != it->second)) return false;
    }
    return true;
  }

  bool operator!=(const TreeMap& other) const
  {
    return !(*this == other);
  }

  iterator begin()
  {
    if (size == 0) return iterator(this,root, true);
    Node* temp = root;

    while (temp->left != nullptr)
    {
      temp = temp->left;
    }

    return iterator(this,temp, false);
  }

  iterator end()
  {
    if (size == 0) return iterator(this,root, true);
    Node* temp = root;

    while (temp->right != nullptr)
    {
      temp = temp->right;
    }

    return iterator(this,temp, true);
  }

  const_iterator cbegin() const
  {
    if (size == 0) return const_iterator(this,root, true);
    Node* temp = root;

    while (temp->left != nullptr)
    {
      temp = temp->left;
    }

    return const_iterator(this,temp, false);
  }

  const_iterator cend() const
  {
    if (size == 0) return const_iterator(this,root, true);
    Node* temp = root;

    while (temp->right != nullptr)
    {
      temp = temp->right;
    }

    return const_iterator(this,temp, true);
  }

  const_iterator begin() const
  {
    return cbegin();
  }

  const_iterator end() const
  {
    return cend();
  }

protected:

  struct Node
  {
    Node* parent;
    Node* left;
    Node* right;
    value_type keyValPair;
    Node(KeyType key, ValueType val):parent(nullptr),left(nullptr),right(nullptr),keyValPair(key,val) {}
  };

  Node* root;
  size_type size;

private:

  Node* find_parent(KeyType key)//find parent of node with key
  {
    Node* temp=root;
    Node* parent= nullptr;

    while(temp!=nullptr)
    {
        if(temp->keyValPair.first==key)
            return parent;

        parent=temp;

        if(temp->keyValPair.first>key)
            temp=temp->left;
        else temp= temp->right;
    }
    return parent;
  }
  void remove_all()
  {
    remove_part(root);
    size = 0;
    root = nullptr;
    return;
  }

  void remove_part(Node* temp)
  {
    if (temp== nullptr) return;

    if (temp->left != nullptr)
    {
      remove_part(temp->left);
    }

    if (temp->right != nullptr)
    {
      remove_part(temp->right);
    }
    --size;
    delete temp;
    return;
  }
  void add(KeyType key,ValueType val)
  {
    Node* new_node = new Node(key,val);
    Node* parent = nullptr;
    if( isEmpty() )
         root=new_node;
    else
    {
        Node* temp;
        temp = root;

        while(temp!= nullptr)
        {
            parent = temp;
            if( new_node->keyValPair.first < temp->keyValPair.first )
                 temp = temp->left;
            else
                 temp = temp->right;

        }
        if( new_node->keyValPair.first < parent->keyValPair.first )
             parent->left = new_node;
        else
             parent->right = new_node;

        new_node->parent = parent;
    }
    ++size;
  }//to add
};

template <typename KeyType, typename ValueType>
class TreeMap<KeyType, ValueType>::ConstIterator
{
friend class TreeMap<KeyType, ValueType>;
public:
  using reference = typename TreeMap::const_reference;
  using iterator_category = std::bidirectional_iterator_tag;
  using value_type = typename TreeMap::value_type;
  using pointer = const typename TreeMap::value_type*;

  explicit ConstIterator(const TreeMap<KeyType,ValueType>* t,Node* n,bool flag):tree(t),node(n),isEnd(flag)
  {}

  ConstIterator(const ConstIterator& other):tree(other.tree)
  {
    node=other.node;
    isEnd=other.isEnd;
  }

  ConstIterator& operator++()
  {
     if (isEnd) throw std::out_of_range("Trying ++ to end ");
     if (tree->isEmpty()) throw std::out_of_range("Trying to ++ empty tree ");
     if(node->parent==nullptr && (node->right)==nullptr)
     {
          isEnd = true;
          return *this;
     }
     if(node->right)
     {
        node=node->right;
        while( node->left )
             node = node->left;
        return *this;
     }
     else
     {
        Node* temp = node->parent;
        if(temp)
        {
            while( temp->right == node)
            {
                node = temp;
                temp = temp->parent;
                if( !temp )
                     break;

            }
            if( !temp)
            {
                while(node->right!=nullptr)
                    node=node->right;
                isEnd=true;
                return *this;
            }
            else
            {
                node= temp;
                return * this;
            }
        }
        isEnd=true;
     }

    return * this;

  }

  ConstIterator operator++(int)
  {
     ConstIterator temp = *this;
     operator++();
     return temp;
  }

  ConstIterator& operator--()
  {

    if(*this==tree->begin()) throw std::out_of_range("trying to -- begin");
    if(node==nullptr)  throw std::out_of_range("trying to -- nullptr");
    if(isEnd)
    {
        if(tree->isEmpty())
            throw std::out_of_range("trying to -- empty tree");
        isEnd=false;
        return* this;
    }
    if (node->left == nullptr)
    {
        while((node->parent != nullptr) && (node->parent->left)==node)
        {
            node=node->parent;
        }
        if(node->parent==nullptr)
        {
            throw std::out_of_range("out of range");
        }
        else
        {
            node=node->parent;
            return *this;
        }

    }
    else
    {
        node = node->left;
        while(node->right != nullptr)
        {
            node = node->right;
        }
      return *this;

    }
  }//operator --

  ConstIterator operator--(int)
  {
     ConstIterator temp = *this;
     operator--();
     return temp;
  }

  reference operator*() const
  {
     if (isEnd) throw std::out_of_range("Trying to to * end");
     return this->node->keyValPair;
  }

  pointer operator->() const
  {
    return &this->operator*();
  }

  bool operator==(const ConstIterator& other) const
  {
    return (isEnd==other.isEnd && node==other.node && tree==other.tree);
  }

  bool operator!=(const ConstIterator& other) const
  {
    return !(*this == other);
  }
  protected:
    const TreeMap<KeyType,ValueType>* tree;
    Node* node;
    bool isEnd;
};

template <typename KeyType, typename ValueType>
class TreeMap<KeyType, ValueType>::Iterator : public TreeMap<KeyType, ValueType>::ConstIterator
{
public:
  using reference = typename TreeMap::reference;
  using pointer = typename TreeMap::value_type*;

  explicit Iterator(const TreeMap<KeyType,ValueType>* t,Node* n,bool flag):ConstIterator(t,n,flag)
  {}

  Iterator(const ConstIterator& other)
    : ConstIterator(other)
  {}

  Iterator& operator++()
  {
    ConstIterator::operator++();
    return *this;
  }

  Iterator operator++(int)
  {
    auto result = *this;
    ConstIterator::operator++();
    return result;
  }

  Iterator& operator--()
  {
    ConstIterator::operator--();
    return *this;
  }

  Iterator operator--(int)
  {
    auto result = *this;
    ConstIterator::operator--();
    return result;
  }

  pointer operator->() const
  {
    return &this->operator*();
  }

  reference operator*() const
  {
    // ugly cast, yet reduces code duplication.
    return const_cast<reference>(ConstIterator::operator*());
  }
};

}

#endif /* AISDI_MAPS_MAP_H */
