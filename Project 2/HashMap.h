#ifndef AISDI_MAPS_HASHMAP_H
#define AISDI_MAPS_HASHMAP_H

#include <cstddef>
#include <initializer_list>
#include <stdexcept>
#include <utility>
#include <functional>
#include"TreeMap.h"
#define BUCKETSNR 101

namespace aisdi
{

template <typename KeyType, typename ValueType>
class HashMap
{
public:
  using key_type = KeyType;
  using mapped_type = ValueType;
  using value_type = std::pair<const key_type, mapped_type>;
  using size_type = std::size_t;
  using reference = value_type&;
  using const_reference = const value_type&;

  class ConstIterator;
  class Iterator;
  using iterator = Iterator;
  using const_iterator = ConstIterator;

  HashMap()
  {
    size=0;
    array = new TreeMap<KeyType, ValueType>[BUCKETSNR+1];
  }
  ~HashMap()
  {
    delete[] array;
  }
  HashMap(std::initializer_list<value_type> list): HashMap()
  {
    std::hash<KeyType> myHash;
    for (auto it = list.begin(); it != list.end(); ++it)
    {
      array[myHash(it->first)%BUCKETSNR][it->first] = it->second;
      ++size;
    }
  }

  HashMap(const HashMap& other): HashMap()
  {
    size=other.size;
    for(int i=0;i<BUCKETSNR;i++)
        array[i]=other.array[i];
  }

  HashMap(HashMap&& other)
  {
    array=other.array;
    size=other.size;
    other.size=0;
    other.array=nullptr;
  }

  HashMap& operator=(const HashMap& other)
  {
    if(this==&other)
        return *this;
    size=other.size;
    for(int i=0;i<BUCKETSNR;i++)
        array[i]=other.array[i];
    return *this;
  }

  HashMap& operator=(HashMap&& other)
  {
    if (this != &other)
    {
      array = other.array;
      size = other.size;
      other.array = nullptr;
      other.size = 0;
    }
    return *this;
  }

  bool isEmpty() const
  {
    return size == 0;
  }

  mapped_type& operator[](const key_type& key)
  {
    std::hash<KeyType> myHash;
    Iterator it= find(key);

    if(it!=end())
        return array[myHash(key)%BUCKETSNR].find(key)->second;
    else
    {
        ++size;
        return array[myHash(key)%BUCKETSNR][key];
    }
  }

  const mapped_type& valueOf(const key_type& key) const
  {
        ConstIterator it= find(key);
        if(it==end())  throw std::out_of_range("Trying to valueOf end ");
        return it->second;
  }

  mapped_type& valueOf(const key_type& key)
  {
        Iterator it= find(key);
        if(it==end())  throw std::out_of_range("Trying to valueOf end ");
        return it->second;
  }

  const_iterator find(const key_type& key) const
  {
    std::hash<KeyType> myHash;
    typename TreeMap<KeyType,ValueType>::Iterator it=array[myHash(key)%BUCKETSNR].find(key);
    if(it!= array[myHash(key)%BUCKETSNR].end())
        return ConstIterator(this,myHash(key)%BUCKETSNR,it);
    return ConstIterator(this,BUCKETSNR,array[BUCKETSNR].begin());
  }

  iterator find(const key_type& key)
  {
    std::hash<KeyType> myHash;
    typename TreeMap<KeyType,ValueType>::Iterator it=array[myHash(key)%BUCKETSNR].find(key);
    if(it!= array[myHash(key)%BUCKETSNR].end())
        return Iterator(this,myHash(key)%BUCKETSNR,it);
    return Iterator(this,BUCKETSNR,array[BUCKETSNR].begin());
  }

  void remove(const key_type& key)
  {
    ConstIterator it = find(key);
    return remove(it);
  }

  void remove(const const_iterator& it)
  {
    std::hash<KeyType> myHash;
    if(it==end()) throw std::out_of_range("trying to remove end");
    if(isEmpty()) throw std::out_of_range("trying to remove from empty HashMap");
    else
    {
        array[myHash(it->first)%BUCKETSNR].remove(it.iter);
        --size;
    }
  }

  size_type getSize() const
  {
    return size;
  }

  bool operator==(const HashMap& other) const
  {
    if(size!=other.size) return false;

    for(Iterator it=other.begin();it!=other.end();++it)
    {
        if(find(it.iter->first)==end() || *(find(it.iter->first))!=*it)
            return false;

    }
    return true;
  }

  bool operator!=(const HashMap& other) const
  {
    return !(*this == other);
  }

  iterator begin()
  {
    return Iterator(cbegin());
  }

  iterator end()
  {
    return Iterator(cend());
  }

  const_iterator cbegin() const
  {
     if(size==0)
        return ConstIterator(this,BUCKETSNR,array[BUCKETSNR].end());
     int i=0;
     while(array[i].isEmpty())
       ++i;
     return ConstIterator(this,i,array[i].begin());
  }

  const_iterator cend() const
  {
    return ConstIterator(this,BUCKETSNR,array[BUCKETSNR].end());
  }

  const_iterator begin() const
  {
    return cbegin();
  }

  const_iterator end() const
  {
    return cend();
  }

  protected:

    TreeMap<KeyType,ValueType>* array;
    int size;

};

template <typename KeyType, typename ValueType>
class HashMap<KeyType, ValueType>::ConstIterator
{
friend class HashMap;
public:
  using reference = typename HashMap::const_reference;
  using iterator_category = std::bidirectional_iterator_tag;
  using value_type = typename HashMap::value_type;
  using pointer = const typename HashMap::value_type*;

  explicit ConstIterator(const HashMap<KeyType,ValueType>* t,int c,typename TreeMap<KeyType,ValueType>::Iterator it):map(t),currentIndex(c),iter(it)
  {}

  ConstIterator(const ConstIterator& other):map(other.map), currentIndex(other.currentIndex), iter(other.iter)
  {}

  ConstIterator& operator++()
  {
    if(currentIndex==BUCKETSNR)
        throw std::out_of_range("Trying to ++ last element of array");

    //iter can be last element or end() we have to move the index for first not empty
    if(iter != map->array[currentIndex].end() || ++iter != map->array [currentIndex].end())
    {
        ++currentIndex;
        if(currentIndex==BUCKETSNR)//end()
                return *this;
        while(map->array[currentIndex].isEmpty())
        {
            ++currentIndex;
            if(currentIndex==BUCKETSNR)//end()
              return *this;
        }
        iter=map->array[currentIndex].begin();
            return *this;
    }
    else
        return *this;
  }

  ConstIterator operator++(int)
  {
    ConstIterator it = *this;
    operator++();
    return it;
  }

  ConstIterator& operator--()
  {
    if(!currentIndex && iter==map->array[currentIndex].begin())
        throw std::out_of_range("Trying to -- begin");

    if(iter==map->array[currentIndex].begin())
    {
        --currentIndex;
        if(!currentIndex && map->array[currentIndex].isEmpty())
            throw std::out_of_range("trying to -- empty bucket");
        while(map->array[currentIndex].isEmpty())
        {
            --currentIndex;
             if(!currentIndex && map->array[currentIndex].isEmpty())
                throw std::out_of_range("trying to -- empty bucket");
        }
        iter=map->array[currentIndex].end();
        --iter;
        return *this;
    }
    else
    {
        --iter;
        return *this;
    }
  }

  ConstIterator operator--(int)
  {
    ConstIterator it = *this;
    operator--();
    return it;
  }

  reference operator*() const
  {
    if(currentIndex==BUCKETSNR)
        throw std::out_of_range("Trying to * last");
    return *iter;
  }

  pointer operator->() const
  {
    return &this->operator*();
  }

  bool operator==(const ConstIterator& other) const
  {
    if(map != other.map)
      return false;
    // have to do this because operator++ dont set frag isEnd
    if (currentIndex == BUCKETSNR && other.currentIndex == BUCKETSNR)
      return true;
    if (currentIndex == other.currentIndex && iter== other.iter)
      return true;

    return false;
  }

  bool operator!=(const ConstIterator& other) const
  {
    return !(*this == other);
  }
  protected:
    const HashMap<KeyType,ValueType>* map;
    int currentIndex;
    typename TreeMap<KeyType,ValueType>::Iterator iter;

};

template <typename KeyType, typename ValueType>
class HashMap<KeyType, ValueType>::Iterator : public HashMap<KeyType, ValueType>::ConstIterator
{
public:
  using reference = typename HashMap::reference;
  using pointer = typename HashMap::value_type*;

  explicit Iterator(const HashMap<KeyType,ValueType>* m,int i, typename TreeMap<KeyType, ValueType>::Iterator it):ConstIterator(m,i,it)
  {}

  Iterator(const ConstIterator& other)
    : ConstIterator(other)
  {}

  Iterator& operator++()
  {
    ConstIterator::operator++();
    return *this;
  }

  Iterator operator++(int)
  {
    auto result = *this;
    ConstIterator::operator++();
    return result;
  }

  Iterator& operator--()
  {
    ConstIterator::operator--();
    return *this;
  }

  Iterator operator--(int)
  {
    auto result = *this;
    ConstIterator::operator--();
    return result;
  }

  pointer operator->() const
  {
    return &this->operator*();
  }

  reference operator*() const
  {
    // ugly cast, yet reduces code duplication.
    return const_cast<reference>(ConstIterator::operator*());
  }
};

}

#endif /* AISDI_MAPS_HASHMAP_H */
